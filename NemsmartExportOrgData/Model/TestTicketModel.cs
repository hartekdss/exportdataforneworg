﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NemsmartTicketQueue.Model
{
    public class StatusName
    {
        public string name { get; set; }
    }
    public class CompanyStatus
    {
        public int id { get; set; }
        public string name { get; set; }
    }
    public class boards
    {
        public string name { get; set; }
        public Status location { get; set; }
        public Status department { get; set; }
    }
    public class CompanyContact
    {
        public string idClientID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactName { get; set; }
        public string PrimaryPhoneNumber { get; set; }
        public string PrimaryEmailAddress { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }

    }
    public class CompanyContactCW
    {
        public Status company { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public Status site { get; set; }
        public List<CommunicationItem> communicationItems = new List<CommunicationItem>();        
        public string addressLine1 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }

    }
    public class CommunicationItem
    {
        public string value { get; set; }
        public bool defaultFlag { get; set; } = true;
        public string communicationType { get; set; }
        public Status type { get; set; }
    }
    public class Company
    {
        public int ContentID { get; set; }
        public int AccountNumber { get; set; }
        public string Identifier { get; set; }
        public string ClientName { get; set; }
        public int StatusID { get; set; }
        public int TypeID { get; set; }      
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string PrimaryPhoneNumber { get; set; }
        public string PrimaryFaxNumber { get; set; }
        public string PrimaryEmailAddress { get; set; }
    }
    public class CompanyCW
    {
    
        public string identifier { get; set; } = "";
        public string name { get; set; }
        public Status status { get; set; }
        public List<Status> types = new List<Status>();
        public Status site { get; set; }
        //  public List<Status> types { get; set; }
        public string phoneNumber { get; set; } = "";
        public string faxNumber { get; set; } = "";

        //public Status defaultContact { get; set; }
        public string addressLine1 { get; set; } = "";
        public string addressLine2 { get; set; } = "";
        public string city { get; set; } = "";
        public string state { get; set; } = "";
        public string zip { get; set; } = "";
        public Status country { get; set; }
        
    }
    public class CompanyCWSite
    {

        
        public string name { get; set; }
        
        public string phoneNumber { get; set; } = "";
        public string faxNumber { get; set; } = "";
         
        public string addressLine1 { get; set; } = "";
        public string addressLine2 { get; set; } = "";
        public string city { get; set; } = "";
        public StateReference stateReference { get; set; }   
        public string zip { get; set; } = "";
        public Status country { get; set; }
        public Status company { get; set; }

    }
    public class StateReference {
        public string identifier { get; set; }   

    }

    public class Status
    {
        public int id { get; set; }
        public string name { get; set; } = "";
    }
    public class OrganizationClient
    {
        public string name { get; set; } = "";
        public int id { get; set; }

    }
    public class SubType
    {
        public string name { get; set; } = "";
        public string typeAssociationIdsNew { get; set; } = "";

    }
    public class NemsmartAsset
    {
        public int id { get; set; }
        public string Name { get; set; }
        public int OrgID { get; set; }
        public int LocationID { get; set; }

        public int ClientID { get; set; }
        public int AssetTypeID { get; set; }
        public string AssetTypeName { get; set; }
        public string SerialNumber { get; set; }
        public string ModelNumber { get; set; }
        public string Tag { get; set; }
        public int ManufacturerID { get; set; }
        public string ManufacturerName { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public Boolean HasUpdated { get; set; }
        public int PSAConfigID { get; set; }

        public string BackupServerName { get; set; } = "";
        public string BackupProtectedDeviceList { get; set; } = "";
        public Boolean BillFlag { get; set; } = true;
        public int BusinessUnitId { get; set; } = 0;
        public string CPUSpeed { get; set; } = "";
        public string DefaultGateway { get; set; } = "";
        public string DeviceIdentifier { get; set; } = "";
        public string InstallationDate { get; set; } = "";
        public string IpAddress { get; set; } = "";
        public string LocalHardDrives { get; set; } = "";
        public string MacAddress { get; set; } = "";
        public string ManagementLink { get; set; } = "";
        public string MobileGuid { get; set; } = "";
        public string Notes { get; set; } = "";
        public string OSInfo { get; set; } = "";
        public string OSType { get; set; } = "";
        public string Ram { get; set; } = "";
        public string RemoteLink { get; set; } = "";
        public string VendorNotes { get; set; } = "";
    }
    public class PSAConfiguration
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public PSAType type { get; set; }
        public string serialNumber { get; set; }
        public string modelNumber { get; set; }
        public string tagNumber { get; set; }
        public StatusForConfigDetails status { get; set; }
        public PSAManufacturer manufacturer { get; set; }
        public StatusForConfigDetails company { get; set; }
        public string BackupServerName { get; set; } = "";
        public string BackupProtectedDeviceList { get; set; } = "";
        public Boolean BillFlag { get; set; } = true;
        public int BusinessUnitId { get; set; } = 0;
        public string CPUSpeed { get; set; } = "";
        public string DefaultGateway { get; set; } = "";
        public string DeviceIdentifier { get; set; } = "";
        public string InstallationDate { get; set; } = "";
        public string IpAddress { get; set; } = "";
        public string LocalHardDrives { get; set; } = "";
        public string MacAddress { get; set; } = "";
        public string ManagementLink { get; set; } = "";
        public string MobileGuid { get; set; } = "";
        public string Notes { get; set; } = "";
        public string OSInfo { get; set; } = "";
        public string OSType { get; set; } = "";
        public string Ram { get; set; } = "";
        public string RemoteLink { get; set; } = "";
        public string VendorNotes { get; set; } = "";
    }
    public class StatusForConfigDetails
    {
        public int id { get; set; }
        public string name { get; set; }
    }
    public class PSAManufacturer
    {
        public int id { get; set; }
        public string name { get; set; }
       // public Boolean inactiveFlag { get; set; }
        
    }
    public class subTypeCW
    {
        public string name { get; set; }
        public Boolean inactiveFlag { get; set; } = false;
        public int[] typeAssociationIds { get; set; }
        public PSAManufacturer board { get; set; }
          
    }
    public class NemsmartManufacturer
    {
        public int Id { get; set; }
        public int OrgID { get; set; }
        public int PsaManufacturerID { get; set; }
        public string ManufacturerName { get; set; }
        public Boolean HasUpdated { get; set; }
    }
    public class PSAType
    {
        public int id { get; set; }
        public string name { get; set; }
        
    }
}

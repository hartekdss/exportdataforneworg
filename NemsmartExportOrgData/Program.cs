﻿using Microsoft.Extensions.Configuration;
using NemsmartTicketQueue.DAL;
using NemsmartTicketQueue.Model;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using static NemsmartTicketQueue.DAL.TestTicketDAL;

namespace NemsmartTicketQueue
{
    class Program
    {
        private static IConfiguration _iconfiguration;
        //  public static string GetTicketListForQueueByCategory = "service/tickets/?conditions={0}&fields=id,company/id,company/phoneNumber,company/name,contact/id,contact/name,agreement/id,agreement/name,priority/id,priority/name,priority/sort,site/id,site/name,summary,status/id,status/name,dateEntered,board,_info,contactName&orderBy=dateEntered DESC&pagesize=500";
        public static string idOrgID = "1";
        public static string OrgName = "Testing Grounds";
        public static string Domain = "core-group.com";
        public static string APIKey = "TlRNK2Z5UTg3VEszTThtM29QOFo6NlFuQUxlaG16ZGRlRUZBMw==";
        public static string APIRoot = "https://api-na.myconnectwise.net/v4_6_release/apis/3.0/";
        public static string ConnectwiseCompany = "2";
        public static string ClientIDKey = "19b59e22-fe18-4f2a-a6e7-3d89f68b9ece";

        private static string GetCompany = "/company/companies";
        private static string GetCompanyStatuses = "/company/companies/statuses";
        private static string GetCompanyTypes = "/company/companies/types";
        private static string GetCompanySite = "/company/companies/{0}/sites";
        public static string GetClientAgreement = "finance/agreements?fields=name,id,endDate,company/id,noEndingDateFlag,cancelledFlag&page={0}&pagesize=100";
        private static string GetClientAgreementUpdate = "finance/agreements?conditions={0}&fields=name,id,endDate,company/id,noEndingDateFlag,cancelledFlag&page={1}&pageSize=100";
        public static string GetCompanySites = "company/companies/{0}/sites?conditions=inactiveFlag=false&fields=id,name,addressLine1,city,state,zip,country/id,country/name,timezone/id,timezone/name";
        public static string GetCompanySitesLastUpdated = "company/companies/{0}/sites?conditions={1} and inactiveFlag=false&fields=id,name,addressLine1,city,state,zip,country/id,country/name,timezone/id,timezone/name";
        public static string GetCompanyContacts = "company/contacts?conditions=company/ID={0} and inactiveflag=false&fields=id,firstname,lastname,company/id,site/id,type/id,type/name,communicationItems,gender,birthDay,relationship/id,childrenFlag,defaultBillingFlag,managerContactId,customFields,inactiveflag&page={1}&pagesize=500";
        public static string GetCompanyContactsUpdate = "company/contacts?conditions={0} and company/ID={1}&fields=id,firstname,lastname,company/id,site/id,type/id,type/name,communicationItems,gender,birthDay,relationship/id,childrenFlag,defaultBillingFlag,managerContactId,customFields,inactiveflag&page={2}&pagesize=500";

        //private static string aa = AppDomain.CurrentDomain.BaseDirectory;
        public static string datePatt = @"yyyy-MM-ddTHH:mm:ssZ";
        static void Main(string[] args)
        {
            GetAppSettingsFile();
            Console.WriteLine("");

            // UpdateCompanyType();
            // AddBoard();
            //AddContact(19300, 11497, "Principium Health, LLC - Dallas");
            // UpdateCompany();
            // AddCompanySite(19393);
            // AddAsset(19301);
            // AddAssetDefault();
            AddEvent();
            //  AddManufacturerName();
        }

        static void AddBoard()
        {
            string endpoint = "/service/boards";
            boards obj = new boards();
            obj.name = "Triage";
            Status obj1 = new Status();
            obj1.id = 11;
            obj.location = obj1;
            obj1 = new Status();
            obj1.id = 1;
            obj.department = obj1;

            string jsonBody = JsonConvert.SerializeObject(obj);

            dynamic result = DoManageAPIRequest<dynamic>(APIRoot, APIKey, endpoint, null, jsonBody: jsonBody, method: Method.POST, Timeout: 60000, clientIDKey: ClientIDKey);


            obj.name = "System Ops";
            jsonBody = JsonConvert.SerializeObject(obj);

            result = DoManageAPIRequest<dynamic>(APIRoot, APIKey, endpoint, null, jsonBody: jsonBody, method: Method.POST, Timeout: 60000, clientIDKey: ClientIDKey);
            obj.name = "Dispatch";
            jsonBody = JsonConvert.SerializeObject(obj);

            result = DoManageAPIRequest<dynamic>(APIRoot, APIKey, endpoint, null, jsonBody: jsonBody, method: Method.POST, Timeout: 60000, clientIDKey: ClientIDKey);


        }
        static void UpdateCompanyStatus()
        {
            TestTicketDAL testdal = new TestTicketDAL(_iconfiguration);
            Console.WriteLine("Company Status Service started... " + DateTime.Now);
            List<CompanyStatus> lstUser = new List<CompanyStatus>();
            lstUser = testdal.GetCompanyStatus();
            string endpoint = GetCompanyStatuses;

            for (int i = 0; i < lstUser.Count; i++)
            {
                StatusName obj = new StatusName();
                obj.name = lstUser[i].name;
                string jsonBody = JsonConvert.SerializeObject(obj);

                CompanyStatus result = DoManageAPIRequest<CompanyStatus>(APIRoot, APIKey, endpoint, null, jsonBody: jsonBody, method: Method.POST, Timeout: 60000, clientIDKey: ClientIDKey);

                testdal.addCompanyStatus(lstUser[i].id, result.id);
            }

            Console.WriteLine("Company Status Service Ended... " + DateTime.Now);
        }

        static void UpdateCompanyType()
        {
            TestTicketDAL testdal = new TestTicketDAL(_iconfiguration);
            Console.WriteLine("Company Type Service started... " + DateTime.Now);
            List<CompanyStatus> lstUser = new List<CompanyStatus>();
            lstUser = testdal.GetCompanyType();
            string endpoint = GetCompanyTypes;

            for (int i = 0; i < lstUser.Count; i++)
            {
                StatusName obj = new StatusName();
                obj.name = lstUser[i].name;
                string jsonBody = JsonConvert.SerializeObject(obj);

                CompanyStatus result = DoManageAPIRequest<CompanyStatus>(APIRoot, APIKey, endpoint, null, jsonBody: jsonBody, method: Method.POST, Timeout: 60000, clientIDKey: ClientIDKey);

                testdal.addCompanyType(lstUser[i].id, result.id);
            }

            Console.WriteLine("Company Type Service Ended... " + DateTime.Now);
        }
        static void UpdateCompany()
        {
            TestTicketDAL testdal = new TestTicketDAL(_iconfiguration);
            Console.WriteLine("Company   Service started... " + DateTime.Now);
            List<Company> lstUser = new List<Company>();
            lstUser = testdal.GetCompanyList();
            string endpoint = GetCompany;

            for (int i = 0; i < lstUser.Count; i++)
            {
                CompanyCW obj = new CompanyCW();
                obj.identifier = lstUser[i].Identifier.Replace("-", "").Replace(".", "").Replace(",", "").Replace("@", "").Replace("&", "").Replace(" ", "").Substring(0, lstUser[i].Identifier.Replace("-", "").Replace(".", "").Replace(",", "").Replace("@", "").Replace("&", "").Replace(" ", "").Length > 24 ? 24 : lstUser[i].Identifier.Replace(".", "").Replace(",", "").Replace("@", "").Replace("-", "").Replace("&", "").Replace(" ", "").Length);
                obj.name = lstUser[i].ClientName.Substring(0, lstUser[i].ClientName.Length > 50 ? 50 : lstUser[i].ClientName.Length);
                if (lstUser[i].Address1 != "")
                    obj.addressLine1 = lstUser[i].Address1;
                if (lstUser[i].Address2 != "")
                    obj.addressLine2 = lstUser[i].Address2;
                if (lstUser[i].City != "")
                    obj.city = lstUser[i].City;
                if (lstUser[i].State != "")
                    obj.state = lstUser[i].State;
                if (lstUser[i].PrimaryPhoneNumber != "")
                    obj.phoneNumber = lstUser[i].PrimaryPhoneNumber;
                else
                {
                    obj.phoneNumber = "111111111111";
                }
                if (lstUser[i].PrimaryFaxNumber != "")
                    obj.faxNumber = lstUser[i].PrimaryFaxNumber;
                Status obj1 = new Status();
                obj1.id = lstUser[i].StatusID;
                //obj1.name = "Active";
                obj.status = obj1;
                obj1 = new Status();
                obj1.id = lstUser[i].TypeID;
                // obj1.name = "Vendor";
                obj.types.Add(obj1);
                obj1 = new Status();
                obj1.id = 1;
                //obj1.name = "United States";
                obj.country = obj1;
                obj1 = new Status();
                obj1.name = "Main";
                obj.site = obj1;
                if (lstUser[i].PostalCode != "")
                    obj.zip = lstUser[i].PostalCode;


                string jsonBody = JsonConvert.SerializeObject(obj);

                CompanyStatus result = DoManageAPIRequest<CompanyStatus>(APIRoot, APIKey, endpoint, null, jsonBody: jsonBody, method: Method.POST, Timeout: 60000, clientIDKey: ClientIDKey);
                if (result != null && result.id > 0)
                {
                    testdal.addCompany(lstUser[i].ContentID, result.id);
                    int contact = AddContact(result.id, lstUser[i].AccountNumber, "Main");
                    AddCompanySite(result.id);
                    AddAgreement(result.id, contact);
                    AddAsset(result.id);
                }
            }

            Console.WriteLine("Company   Service Ended... " + DateTime.Now);
        }
        static void AddAgreement(int clientID, int ContactID)
        {
            TestTicketDAL testdal = new TestTicketDAL(_iconfiguration);

            //         string obj=  "{"name":"NS Agreement","contact":{ "id":18},"company":{ "id":19300},"type":{ "id":29}, "startDate": "2022-01-18T09:35:31Z","applicationLimit":0,"applicationUnlimitedFlag":true,"site":{ "name":"Main"},
            //"noEndingDateFlag": true}
            string endpoint = "/finance/agreements";
            string jsonBody = "{\"name\":\"NS Agreement\",\"contact\":{\"id\":\"" + ContactID.ToString() + "\"},\"company\":{\"id\":" + clientID.ToString() + "},\"type\":{\"id\":29},\"site\":{\"name\":\"Main\"},\"applicationUnlimitedFlag\":\"true\",\"noEndingDateFlag\":\"true\",\"startDate\": \"2022-01-18T09:35:31Z\",\"applicationLimit\":0}"; //\"impact\":0,\"severity\":0,

            dynamic result = DoManageAPIRequest<dynamic>(APIRoot, APIKey, endpoint, null, jsonBody: jsonBody, method: Method.POST, Timeout: 60000, clientIDKey: ClientIDKey);


            Console.WriteLine("");
            //}
        }
        static void AddCompanySite(int clientid)
        {
            TestTicketDAL testdal = new TestTicketDAL(_iconfiguration);
            //Console.WriteLine("Company   Service started... " + DateTime.Now);
            List<Company> lstUser = new List<Company>();
            lstUser = testdal.GetCompanySiteList(clientid);
            string endpoint = string.Format(GetCompanySite, clientid);

            for (int i = 0; i < lstUser.Count; i++)
            {
                CompanyCWSite obj = new CompanyCWSite();
                Status obj1 = new Status();
                obj1.id = clientid;
                obj.company = obj1;
                obj.name = lstUser[i].ClientName.Replace("OSM - Ophthalmic Specialists of Michigan - ", "");
                if (lstUser[i].Address1 != "")
                    obj.addressLine1 = lstUser[i].Address1;
                if (lstUser[i].Address2 != "")
                    obj.addressLine2 = lstUser[i].Address2;
                if (lstUser[i].City != "")
                    obj.city = lstUser[i].City;
                if (lstUser[i].State != "")
                {
                    StateReference obj2 = new StateReference();
                    obj2.identifier = lstUser[i].State;
                    obj.stateReference = obj2;
                }
                //if (lstUser[i].State != "")
                //    obj.state = lstUser[i].State;
                if (lstUser[i].PrimaryPhoneNumber != "")
                    obj.phoneNumber = lstUser[i].PrimaryPhoneNumber;
                if (lstUser[i].PrimaryFaxNumber != "")
                    obj.faxNumber = lstUser[i].PrimaryFaxNumber;


                obj1 = new Status();
                obj1.id = 1;
                obj.country = obj1;

                if (lstUser[i].PostalCode != "")
                    obj.zip = lstUser[i].PostalCode;


                string jsonBody = JsonConvert.SerializeObject(obj);

                dynamic result = DoManageAPIRequest<dynamic>(APIRoot, APIKey, endpoint, null, jsonBody: jsonBody, method: Method.POST, Timeout: 60000, clientIDKey: ClientIDKey);
                if (result != null && result.id > 0)
                {
                    AddContact(clientid, lstUser[i].AccountNumber, lstUser[i].ClientName);

                }
                Console.WriteLine("");
            }

            // Console.WriteLine("Company   Service Ended... " + DateTime.Now);
        }
        static int AddContact(int ClientID, int AccountNumber, string sitename = "Main")
        {
            TestTicketDAL testdal = new TestTicketDAL(_iconfiguration);
            int contactid = 0;
            string endpoint = "/company/contacts";
            List<CompanyContact> list = new List<CompanyContact>();
            list = testdal.GetCompanyContactList(AccountNumber);

            for (int i = 0; i < list.Count; i++)
            {
                CompanyContactCW obj = new CompanyContactCW();

                obj.addressLine1 = list[i].Address1;
                obj.firstName = list[i].FirstName;
                obj.lastName = list[i].LastName;
                obj.city = list[i].City;
                obj.zip = list[i].PostalCode;
                Status obj1 = new Status();
                obj1.id = ClientID;
                obj.company = obj1;

                obj1 = new Status();
                obj1.name = sitename;
                obj.site = obj1;
                CommunicationItem com = new CommunicationItem();
                if (list[i].PrimaryEmailAddress != "")
                {
                    Status obj2 = new Status();
                    obj2.id = 1;
                    com.communicationType = "Email";
                    com.value = list[i].PrimaryEmailAddress;
                    com.type = obj2;
                    obj.communicationItems.Add(com);
                }
                if (list[i].PrimaryPhoneNumber != "")
                {
                    com = new CommunicationItem();
                    Status obj2 = new Status();
                    obj2.id = 2;
                    com.type = obj2;
                    com.communicationType = "Phone";
                    com.value = list[i].PrimaryPhoneNumber;
                    obj.communicationItems.Add(com);
                }


                string jsonBody = JsonConvert.SerializeObject(obj);

                dynamic result = DoManageAPIRequest<dynamic>(APIRoot, APIKey, endpoint, null, jsonBody: jsonBody, method: Method.POST, Timeout: 60000, clientIDKey: ClientIDKey);
                if (i == 0 && result != null && result.id > 0)
                {
                    contactid = result.id;
                }

                Console.WriteLine("");
            }
            return contactid;

        }
        static void AddManufacturerName()
        {
            TestTicketDAL testdal = new TestTicketDAL(_iconfiguration);

            string endpoint = "/procurement/manufacturers";
            List<string> list = new List<string>();
            list = testdal.GetManufacturerName();

            for (int i = 0; i < list.Count; i++)
            {
                StatusName obj = new StatusName();
                obj.name = list[i];

                string jsonBody = JsonConvert.SerializeObject(obj);

                dynamic result = DoManageAPIRequest<dynamic>(APIRoot, APIKey, endpoint, null, jsonBody: jsonBody, method: Method.POST, Timeout: 60000, clientIDKey: ClientIDKey);
                Console.WriteLine("");
            }


        }
        static void AddAssetType()
        {
            TestTicketDAL testdal = new TestTicketDAL(_iconfiguration);

            string endpoint = "/company/configurations/types";
            List<string> list = new List<string>();
            list = testdal.GetAssetType();

            for (int i = 0; i < list.Count; i++)
            {
                StatusName obj = new StatusName();
                obj.name = list[i];

                string jsonBody = JsonConvert.SerializeObject(obj);

                dynamic result = DoManageAPIRequest<dynamic>(APIRoot, APIKey, endpoint, null, jsonBody: jsonBody, method: Method.POST, Timeout: 60000, clientIDKey: ClientIDKey);
                Console.WriteLine("");
            }


        }
        static void AddAsset(int ClientID)
        {
            TestTicketDAL testdal = new TestTicketDAL(_iconfiguration);

            string endpoint = "/company/configurations";
            List<NemsmartAsset> list = new List<NemsmartAsset>();
            list = testdal.GetAsset(ClientID);
            if (list.Count == 0)
            {
                PSAConfiguration obj = new PSAConfiguration();
                obj.Name = "NS Asset";


                PSAType typ = new PSAType();
                typ.id = 52;
                obj.type = typ;
                StatusForConfigDetails sts = new StatusForConfigDetails();
                sts.id = 2;
                obj.status = sts;
                StatusForConfigDetails cmpy = new StatusForConfigDetails();
                cmpy.id = ClientID;
                obj.company = cmpy;
                string jsonBody = JsonConvert.SerializeObject(obj);

                dynamic result = DoManageAPIRequest<dynamic>(APIRoot, APIKey, endpoint, null, jsonBody: jsonBody, method: Method.POST, Timeout: 60000, clientIDKey: ClientIDKey);

            }
            else
            {
                for (int i = 0; i < list.Count; i++)
                {
                    PSAConfiguration obj = new PSAConfiguration();
                    obj.Name = list[i].Name;
                    obj.CPUSpeed = list[i].CPUSpeed;
                    obj.Notes = list[i].Description;
                    obj.InstallationDate = Convert.ToDateTime(list[i].InstallationDate).ToString("yyyy-MM-ddTHH:mm:ssZ");
                    obj.IpAddress = list[i].IpAddress;
                    obj.LocalHardDrives = list[i].LocalHardDrives;
                    obj.MacAddress = list[i].MacAddress;
                    PSAManufacturer mf = new PSAManufacturer();
                    mf.name = list[i].ManufacturerName;

                    obj.manufacturer = mf;
                    obj.modelNumber = list[i].ModelNumber;
                    obj.OSInfo = list[i].OSInfo;
                    obj.Ram = list[i].Ram;
                    obj.serialNumber = list[i].SerialNumber;
                    PSAType typ = new PSAType();
                    typ.name = list[i].AssetTypeName;
                    obj.type = typ;
                    StatusForConfigDetails sts = new StatusForConfigDetails();
                    sts.id = 2;
                    obj.status = sts;
                    StatusForConfigDetails cmpy = new StatusForConfigDetails();
                    cmpy.id = ClientID;
                    obj.company = cmpy;

                    string jsonBody = JsonConvert.SerializeObject(obj);

                    dynamic result = DoManageAPIRequest<dynamic>(APIRoot, APIKey, endpoint, null, jsonBody: jsonBody, method: Method.POST, Timeout: 60000, clientIDKey: ClientIDKey);
                    //Console.WriteLine("");
                }

            }
        }
        static void AddAssetDefault()
        {
            TestTicketDAL testdal = new TestTicketDAL(_iconfiguration);

            string endpoint = "/company/configurations";
            //  List<NemsmartAsset> list = new List<NemsmartAsset>();
            // list = testdal.GetAsset( );
            List<string> list = new List<string>();
            string clientidlst = "36,55,70,81,114,120,156,192,201,232,310,349,372,378,485,523,631,653,672,710,742,764,790,845,863,944,952,979,997,1051,1091,1302,1567,1593,1854,1910,1921,1956,1989,1999,2002,2035,2093,2112,2208,2244,2257,2293,2310,2321,2331,2334,2340";
            list = clientidlst.Split(',').ToList();

            for (int i = 0; i < list.Count; i++)
            {
                PSAConfiguration obj = new PSAConfiguration();
                obj.Name = "NS_Asset";


                PSAType typ = new PSAType();
                typ.id = 180;
                obj.type = typ;
                StatusForConfigDetails sts = new StatusForConfigDetails();
                sts.id = 1;
                obj.status = sts;
                StatusForConfigDetails cmpy = new StatusForConfigDetails();
                cmpy.id = Convert.ToInt32(list[i]);
                obj.company = cmpy;
                string jsonBody = JsonConvert.SerializeObject(obj);

                dynamic result = DoManageAPIRequest<dynamic>(APIRoot, APIKey, endpoint, null, jsonBody: jsonBody, method: Method.POST, Timeout: 60000, clientIDKey: ClientIDKey);

                Console.WriteLine("");
            }


        }
        static void AddEvent()
        {
            TestTicketDAL testdal = new TestTicketDAL(_iconfiguration);

            string endpoint = "service/boards/23/subtypes";
            List<SubType> list = new List<SubType>();
            list = testdal.GetSubType();
             

            for (int i = 0; i < list.Count; i++)
            {
                subTypeCW obj = new subTypeCW();
                obj.name = list[i].name;

                PSAManufacturer typ = new PSAManufacturer();
                typ.id = 23;
                obj.board = typ;
                obj.typeAssociationIds = list[i].typeAssociationIdsNew.Split(',').Select(Int32.Parse).ToArray() ;
                 
                string jsonBody = JsonConvert.SerializeObject(obj);

                dynamic result = DoManageAPIRequest<dynamic>(APIRoot, APIKey, endpoint, null, jsonBody: jsonBody, method: Method.POST, Timeout: 60000, clientIDKey: ClientIDKey);

                Console.WriteLine("");
            }


        }

        static void GetAppSettingsFile()
        {
            try
            {
                string path1 = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string directoryName = Path.GetDirectoryName(path1);
                var builder = new ConfigurationBuilder()
                .SetBasePath(directoryName)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

                _iconfiguration = builder.Build();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                ErrorWriteLog(DateTime.Now.ToString() + " " + ex.ToString());
            }
            finally { }
        }
        public static T DoManageAPIRequest<T>(string APIURL, string APIKey, string endpoint, Dictionary<string, string> queryParameters = null, string jsonBody = null, Method method = Method.POST, int Timeout = 45000, string clientIDKey = "")
        {
            try
            {

                //string root = string.Format(AppSettings.Apiroot, root);
                endpoint = APIURL + endpoint;
                //  DateTime startTimeOfRequest = DateTime.Now;
                // DateTime endTimeOfRequest = DateTime.MinValue;
                System.Net.ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;

                var client = new RestClient(endpoint);
                var request = new RestRequest(method);

                request.AddHeader("content-type", "application/json");
                // if(endpoint==root+ "service/tickets")
                request.AddHeader("authorization", $"Basic {APIKey}");
                // else
                //request.AddHeader("authorization", $"Basic {apiKey}");
                if (!string.IsNullOrEmpty(clientIDKey))
                {
                    request.AddHeader("clientid", $"{clientIDKey}");
                }

                if (queryParameters != null)
                {
                    foreach (var item in queryParameters)
                    {
                        request.AddQueryParameter(item.Key, item.Value);
                    }
                }

                if (!(string.IsNullOrEmpty(jsonBody)))
                {
                    request.AddParameter("application/json", jsonBody, ParameterType.RequestBody);
                }

                request.Timeout = 30000;// Timeout;

                IRestResponse response = client.Execute(request);
                ////Loop if timeout
                int counter = 0;
                while (((int)response.StatusCode < 200 || (int)response.StatusCode >= 300) && counter < 1)
                {
                    //logit($"Api Call failed,{endpoint},response.StatusCode = {response.StatusCode}, request.Timeout = {request.Timeout}");

                    counter += 1;
                    request.Timeout = 45000;// request.Timeout * counter;
                    response = client.Execute(request);
                }

                // endTimeOfRequest = DateTime.Now;

                //    TimeSpan duration = endTimeOfRequest - startTimeOfRequest;

                //logit($"Api Call took = {duration.TotalMilliseconds} Milliseconds to return., {endpoint}");

                // m_toolHost?.Logger?.LogMessage($"Api Call took {DateDiff(DateInterval.Second, startTimeOfRequest, endTimeOfRequest)} seconds To Return., {endpoint}")
                if ((int)response.StatusCode == 200 || (int)response.StatusCode == 201)
                {
                    // var test = JsonConvert.DeserializeObject<T>(response.Content);
                    return JsonConvert.DeserializeObject<T>(response.Content);
                }
                else
                {
                    return default(T);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                //  Debug.WriteLine($"Ex = {ex.Message}");
                ErrorWriteLog(DateTime.Now.ToString() + " endpoint:- " + endpoint + " " + ex.ToString());
                return default(T);
            }
            finally { }
        }

        public static void ErrorWriteLogGeneric(string strLogText, string module)
        {
            try
            {
                string path1 = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string directoryName = Path.GetDirectoryName(path1);

                string TRACE_LOG_FOLDER = directoryName + "\\" + module + "_Errorlog\\";
                int logday = 10;
                try
                {
                    List<string> LogPurgeDays = _iconfiguration.GetSection("LogPurgeDays:Day").Get<List<string>>();
                    logday = Convert.ToInt32(LogPurgeDays[0]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally { }
                StreamWriter log;
                if (!Directory.Exists(TRACE_LOG_FOLDER))
                {
                    Directory.CreateDirectory(TRACE_LOG_FOLDER);
                }
                if (!File.Exists(TRACE_LOG_FOLDER + DateTime.Now.Date.ToString().Replace("/", "-").Substring(0, 10) + "_logfile.log"))
                {
                    log = new StreamWriter(TRACE_LOG_FOLDER + DateTime.Now.Date.ToString().Replace("/", "-").Substring(0, 10) + "_logfile.log");
                }
                else
                {
                    log = File.AppendText(TRACE_LOG_FOLDER + DateTime.Now.Date.ToString().Replace("/", "-").Substring(0, 10) + "_logfile.log");
                }
                PurgeTraceLogFile(TRACE_LOG_FOLDER, Convert.ToInt32(logday));

                log.WriteLine(strLogText);
                log.Flush();

                // Close the stream:
                log.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            { }
        }

        public static void ErrorWriteLog(string strLogText)
        {
            try
            {
                string path1 = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string directoryName = Path.GetDirectoryName(path1);

                string TRACE_LOG_FOLDER = directoryName + "\\TicketQueue_Errorlog\\";
                int logday = 10;
                try
                {
                    List<string> LogPurgeDays = _iconfiguration.GetSection("LogPurgeDays:Day").Get<List<string>>();
                    logday = Convert.ToInt32(LogPurgeDays[0]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally { }
                StreamWriter log;
                if (!Directory.Exists(TRACE_LOG_FOLDER))
                {
                    Directory.CreateDirectory(TRACE_LOG_FOLDER);
                }
                if (!File.Exists(TRACE_LOG_FOLDER + DateTime.Now.Date.ToString().Replace("/", "-").Substring(0, 10) + "_logfile.log"))
                {
                    log = new StreamWriter(TRACE_LOG_FOLDER + DateTime.Now.Date.ToString().Replace("/", "-").Substring(0, 10) + "_logfile.log");
                }
                else
                {
                    log = File.AppendText(TRACE_LOG_FOLDER + DateTime.Now.Date.ToString().Replace("/", "-").Substring(0, 10) + "_logfile.log");
                }
                PurgeTraceLogFile(TRACE_LOG_FOLDER, Convert.ToInt32(logday));

                log.WriteLine(strLogText);
                log.Flush();

                // Close the stream:
                log.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            { }
        }

        public static void TraceWriteLog(string strLogText)
        {
            try
            {

                int logday = 10;
                try
                {
                    List<string> LogPurgeDays = _iconfiguration.GetSection("LogPurgeDays:Day").Get<List<string>>();
                    logday = Convert.ToInt32(LogPurgeDays[0]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally { }
                try
                {
                    List<string> TraceEnabled = _iconfiguration.GetSection("TraceEnabled:Value").Get<List<string>>();

                    if (TraceEnabled[0].ToLower() != "true")
                    {
                        return;
                    }
                }
                finally { }
                string path1 = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string directoryName = Path.GetDirectoryName(path1);

                string TRACE_LOG_FOLDER = directoryName + "\\TicketQueue_Tracelog\\";

                StreamWriter log;
                if (!Directory.Exists(TRACE_LOG_FOLDER))
                {
                    Directory.CreateDirectory(TRACE_LOG_FOLDER);
                }
                if (!File.Exists(TRACE_LOG_FOLDER + DateTime.Now.Date.ToString().Replace("/", "-").Substring(0, 10) + "_logfile.log"))
                {
                    log = new StreamWriter(TRACE_LOG_FOLDER + DateTime.Now.Date.ToString().Replace("/", "-").Substring(0, 10) + "_logfile.log");
                }
                else
                {
                    log = File.AppendText(TRACE_LOG_FOLDER + DateTime.Now.Date.ToString().Replace("/", "-").Substring(0, 10) + "_logfile.log");
                }
                PurgeTraceLogFile(TRACE_LOG_FOLDER, Convert.ToInt32(logday));

                log.WriteLine(strLogText);
                log.Flush();


                // Close the stream:
                log.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally { }

        }

        public static void TraceWriteLogGeneric(string strLogText, string module)
        {
            try
            {

                int logday = 10;
                try
                {
                    List<string> LogPurgeDays = _iconfiguration.GetSection("LogPurgeDays:Day").Get<List<string>>();
                    logday = Convert.ToInt32(LogPurgeDays[0]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally { }
                try
                {
                    List<string> TraceEnabled = _iconfiguration.GetSection("TraceEnabled:Value").Get<List<string>>();

                    if (TraceEnabled[0].ToLower() != "true")
                    {
                        return;
                    }
                }
                finally { }
                string path1 = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string directoryName = Path.GetDirectoryName(path1);

                string TRACE_LOG_FOLDER = directoryName + "\\" + module + "_Tracelog\\";

                StreamWriter log;
                if (!Directory.Exists(TRACE_LOG_FOLDER))
                {
                    Directory.CreateDirectory(TRACE_LOG_FOLDER);
                }
                if (!File.Exists(TRACE_LOG_FOLDER + DateTime.Now.Date.ToString().Replace("/", "-").Substring(0, 10) + "_logfile.log"))
                {
                    log = new StreamWriter(TRACE_LOG_FOLDER + DateTime.Now.Date.ToString().Replace("/", "-").Substring(0, 10) + "_logfile.log");
                }
                else
                {
                    log = File.AppendText(TRACE_LOG_FOLDER + DateTime.Now.Date.ToString().Replace("/", "-").Substring(0, 10) + "_logfile.log");
                }
                PurgeTraceLogFile(TRACE_LOG_FOLDER, Convert.ToInt32(logday));

                log.WriteLine(strLogText);
                log.Flush();


                // Close the stream:
                log.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally { }

        }


        public static void PurgeTraceLogFile(string filepath, int day = 10)
        {
            string FileFolder = filepath;
            try
            {

                string[] FileList = Directory.GetFiles(FileFolder, "*.log");
                try
                {
                    foreach (string LogFile in FileList)
                    {
                        FileInfo LogInfo = new FileInfo(LogFile);
                        if (LogInfo.LastWriteTime < DateTime.Now.AddDays(-day))
                        {
                            LogInfo.Delete();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally
                {

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally { }
        }

    }
}

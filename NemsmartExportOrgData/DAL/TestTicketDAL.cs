﻿using Microsoft.Extensions.Configuration;
using NemsmartTicketQueue.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace NemsmartTicketQueue.DAL
{
    class TestTicketDAL
    {
        private string _connectionString;
        public TestTicketDAL(IConfiguration iconfiguration)
        {
            _connectionString = iconfiguration.GetConnectionString("Default");
        }
         
       
        public List<CompanyStatus> GetCompanyStatus()
        {
            List<CompanyStatus> lst = new List<CompanyStatus>();
            int i = 0;
            retry:

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    SqlCommand cmd = new SqlCommand("sp_Get_tpaw_Organization_Client_Status", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandTimeout = 60;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        CompanyStatus obj = new CompanyStatus();

                        obj.id = Convert.ToInt32(rdr["idContentID"]);
                        obj.name = Convert.ToString(rdr["cdStatusName"]);

                        lst.Add(obj);
                    }

                }
            }

            catch (SqlException ex)
            {
                if (i < 3)//&& ex.Number == -2           
                {
                    i++;
                    goto retry;
                }


            }
            catch (Exception ex)
            {

            }
            finally { }
            return lst;
        }
        public List<CompanyStatus> GetCompanyType()
        {
            List<CompanyStatus> lst = new List<CompanyStatus>();
            int i = 0;
            retry:

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    SqlCommand cmd = new SqlCommand("sp_Get_tpaw_Organization_Client_Type", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandTimeout = 60;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        CompanyStatus obj = new CompanyStatus();

                        obj.id = Convert.ToInt32(rdr["idContentID"]);
                        obj.name = Convert.ToString(rdr["cdTypeName"]);

                        lst.Add(obj);
                    }

                }
            }

            catch (SqlException ex)
            {
                if (i < 3)//&& ex.Number == -2           
                {
                    i++;
                    goto retry;
                }


            }
            catch (Exception ex)
            {

            }
            finally { }
            return lst;
        }

        public void addCompanyStatus(int ContentID, int StatusID)
        {
            int i = 0;
            retry:

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    SqlCommand cmd = new SqlCommand("sp_Update_tpaw_Organization_Client_Status", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("iiCondentID", SqlDbType.Int).Value = ContentID;
                    cmd.Parameters.Add("iiStatusID", SqlDbType.Int).Value = StatusID;  
                    cmd.CommandTimeout = 60;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    con.Close();
                    con.Dispose();
                }
            }
            catch
            {
                if (i < 3)//&& ex.Number == -2           
                {
                    i++;
                    goto retry;
                }


            }


        }
        public void addCompanyType(int ContentID, int TypeID)
        {
            int i = 0;
            retry:

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    SqlCommand cmd = new SqlCommand("sp_Update_tpaw_Organization_Client_Type", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("iiCondentID", SqlDbType.Int).Value = ContentID;
                    cmd.Parameters.Add("iiTypeID", SqlDbType.Int).Value = TypeID;
                    cmd.CommandTimeout = 60;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    con.Close();
                    con.Dispose();
                }
            }
            catch
            {
                if (i < 3)//&& ex.Number == -2           
                {
                    i++;
                    goto retry;
                }


            }


        }
        public List<Company> GetCompanyList()
        {
            List<Company> lst = new List<Company>();
            int i = 0;
            retry:

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    SqlCommand cmd = new SqlCommand("sp_Get_tpaw_Organization_Client", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandTimeout = 60;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        Company obj = new Company();

                        obj.ContentID = Convert.ToInt32(rdr["idContentID"]);
                        obj.AccountNumber = Convert.ToInt32(rdr["AccountNumber"]);
                        obj.Identifier = Convert.ToString(rdr["Identifier"]);
                        obj.ClientName = Convert.ToString(rdr["cdClientName"]);
                        obj.StatusID = Convert.ToInt32(rdr["idStatusID"]);
                        obj.TypeID = Convert.ToInt32(rdr["idTypeID"]);
                        obj.Address1 = Convert.ToString(rdr["Address1"]);
                        obj.Address2 = Convert.ToString(rdr["Address2"]);
                        obj.City = Convert.ToString(rdr["City"]);
                        obj.State = Convert.ToString(rdr["State"]);
                        obj.PostalCode = Convert.ToString(rdr["PostalCode"]);
                        obj.PrimaryPhoneNumber = Convert.ToString(rdr["PrimaryPhoneNumber"]);
                        obj.PrimaryFaxNumber = Convert.ToString(rdr["PrimaryFaxNumber"]);
                        obj.PrimaryEmailAddress = Convert.ToString(rdr["PrimaryEmailAddress"]);

                        lst.Add(obj);
                    }

                }
            }

            catch (SqlException ex)
            {
                if (i < 3)//&& ex.Number == -2           
                {
                    i++;
                    goto retry;
                }


            }
            catch (Exception ex)
            {

            }
            finally { }
            return lst;
        }
        public List<Company> GetCompanySiteList(int ClientID)
        {
            List<Company> lst = new List<Company>();
            int i = 0;
            retry:

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    SqlCommand cmd = new SqlCommand("sp_Get_tpaw_Organization_Client_Site", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("iiClientID", SqlDbType.Int).Value = ClientID;
                    cmd.CommandTimeout = 60;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        Company obj = new Company();

                        obj.AccountNumber = Convert.ToInt32(rdr["AccountNumber"]);
                      //  obj.Identifier = Convert.ToString(rdr["Identifier"]);
                        obj.ClientName = Convert.ToString(rdr["cdClientName"]);
                        //obj.StatusID = Convert.ToInt32(rdr["idStatusID"]);
                        //obj.TypeID = Convert.ToInt32(rdr["idTypeID"]);
                        obj.Address1 = Convert.ToString(rdr["Address1"]);
                        obj.Address2 = Convert.ToString(rdr["Address2"]);
                        obj.City = Convert.ToString(rdr["City"]);
                        obj.State = Convert.ToString(rdr["State"]);
                        obj.PostalCode = Convert.ToString(rdr["PostalCode"]);
                        obj.PrimaryPhoneNumber = Convert.ToString(rdr["PrimaryPhoneNumber"]);
                        obj.PrimaryFaxNumber = Convert.ToString(rdr["PrimaryFaxNumber"]);
                        obj.PrimaryEmailAddress = Convert.ToString(rdr["PrimaryEmailAddress"]);

                        lst.Add(obj);
                    }

                }
            }

            catch (SqlException ex)
            {
                if (i < 3)//&& ex.Number == -2           
                {
                    i++;
                    goto retry;
                }


            }
            catch (Exception ex)
            {

            }
            finally { }
            return lst;
        }
        
        public List<string> GetManufacturerName()
        {
            List<string> lst = new List<string>();
            int i = 0;
            retry:

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    SqlCommand cmd = new SqlCommand("sp_Get_tpaw_ManufacturerName", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandTimeout = 60;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    { 
                        lst.Add(Convert.ToString(rdr["ManufacturerName"]));
                    }

                }
            }

            catch (SqlException ex)
            {
                if (i < 3)//&& ex.Number == -2           
                {
                    i++;
                    goto retry;
                }


            }
            catch (Exception ex)
            {

            }
            finally { }
            return lst;
        }
        public List<string> GetAssetType()
        {
            List<string> lst = new List<string>();
            int i = 0;
            retry:

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    SqlCommand cmd = new SqlCommand("sp_Get_tpaw_AssetType", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandTimeout = 60;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        lst.Add(Convert.ToString(rdr["AssetType"]));
                    }

                }
            }

            catch (SqlException ex)
            {
                if (i < 3)//&& ex.Number == -2           
                {
                    i++;
                    goto retry;
                }


            }
            catch (Exception ex)
            {

            }
            finally { }
            return lst;
        }
        public List<NemsmartAsset> GetAsset(int ClientID)
        {
            List<NemsmartAsset> lst = new List<NemsmartAsset>();
            int i = 0;
            retry:

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    SqlCommand cmd = new SqlCommand("sp_Get_Asset", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("iiClientID", SqlDbType.Int).Value = ClientID;
                    cmd.CommandTimeout = 60;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        NemsmartAsset OBJ = new NemsmartAsset();
                        OBJ.AssetTypeName = Convert.ToString(rdr["AssetType"]);
                       // OBJ.BackupProtectedDeviceList = Convert.ToString(rdr["AssetType"]);
                      //  OBJ.BackupServerName = Convert.ToString(rdr["AssetType"]);
                       // OBJ.BillFlag = Convert.ToString(rdr["AssetType"]);
                       // OBJ.BusinessUnitId = Convert.ToString(rdr["AssetType"]);
                        
                        OBJ.CPUSpeed = Convert.ToString(rdr["CPU"]);
                       // OBJ.DefaultGateway = Convert.ToString(rdr["AssetType"]);
                        OBJ.Description = Convert.ToString(rdr["Description"]);
                      //  OBJ.DeviceIdentifier = Convert.ToString(rdr["AssetType"]);
                        OBJ.InstallationDate = Convert.ToString(rdr["DateAdded"]);
                        OBJ.IpAddress = Convert.ToString(rdr["IPAddress"]);
                        OBJ.LocalHardDrives = Convert.ToString(rdr["HardDriveSpace"]);
                        // OBJ.LocationID = Convert.ToString(rdr["AssetType"]);
                        OBJ.MacAddress = Convert.ToString(rdr["MACAddress"]);
                      //  OBJ.ManagementLink = Convert.ToString(rdr["AssetType"]);
                       // OBJ.ManufacturerID = Convert.ToString(rdr["AssetType"]);
                        OBJ.ManufacturerName = Convert.ToString(rdr["Manufacturer"]);
                        //OBJ.MobileGuid = Convert.ToString(rdr["AssetType"]);
                        OBJ.ModelNumber = Convert.ToString(rdr["SystemModelNumber"]);
                        OBJ.Name = Convert.ToString(rdr["ProvidersAssetName"]);
                      //  OBJ.Notes = Convert.ToString(rdr["AssetType"]);
                        OBJ.OSInfo = Convert.ToString(rdr["OS"]);
                        //  OBJ.PSAConfigID = Convert.ToString(rdr["AssetType"]);
                          OBJ.Ram = Convert.ToString(rdr["Memory"]);
                        //  OBJ.RemoteLink = Convert.ToString(rdr["AssetType"]);
                        OBJ.SerialNumber = Convert.ToString(rdr["SerialNumber"]);
                      //  OBJ.Status = Convert.ToString(rdr["AssetType"]);
                       // OBJ.Tag = Convert.ToString(rdr["AssetType"]);
                       // OBJ.VendorNotes = Convert.ToString(rdr["AssetType"]);
                         
                        lst.Add(OBJ);
                    }

                }
            }

            catch (SqlException ex)
            {
                if (i < 3)//&& ex.Number == -2           
                {
                    i++;
                    goto retry;
                }


            }
            catch (Exception ex)
            {

            }
            finally { }
            return lst;
        }
        public List<SubType> GetSubType()
        {
            List<SubType> lst = new List<SubType>();
            int i = 0;
            retry:

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    SqlCommand cmd = new SqlCommand("getSubType", con);
                    cmd.CommandType = CommandType.StoredProcedure;                    
                    cmd.CommandTimeout = 60;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        SubType OBJ = new SubType();
                        OBJ.name = Convert.ToString(rdr["cdName"]);
                        OBJ.typeAssociationIdsNew = Convert.ToString(rdr["typeAssociationIdsNew"]);

                        lst.Add(OBJ);
                    }

                }
            }

            catch (SqlException ex)
            {
                if (i < 3)//&& ex.Number == -2           
                {
                    i++;
                    goto retry;
                }


            }
            catch (Exception ex)
            {

            }
            finally { }
            return lst;
        }

        public List<CompanyContact> GetCompanyContactList(int ClientID)
        {
            List<CompanyContact> lst = new List<CompanyContact>();
            int i = 0;
            retry:

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    SqlCommand cmd = new SqlCommand("sp_Get_tpaw_Organization_Client_Contact", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("iiAccountNumber", SqlDbType.Int).Value = ClientID;
                    cmd.CommandTimeout = 60;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        CompanyContact obj = new CompanyContact();

                        
                      //  obj.idClientID = Convert.ToString(rdr["idClientID"]);
                        obj.Address1 = Convert.ToString(rdr["Address1"]);
                        obj.City = Convert.ToString(rdr["City"]);
                        obj.ContactName = Convert.ToString(rdr["ContactName"]);
                        obj.FirstName = Convert.ToString(rdr["FirstName"]);
                        obj.LastName = Convert.ToString(rdr["LastName"]);
                        obj.PostalCode = Convert.ToString(rdr["PostalCode"]);
                        obj.PrimaryEmailAddress = Convert.ToString(rdr["PrimaryEmailAddress"]);
                        obj.PrimaryPhoneNumber = Convert.ToString(rdr["PrimaryPhoneNumber"]);
                        obj.State = Convert.ToString(rdr["State"]);
                         

                        lst.Add(obj);
                    }

                }
            }

            catch (SqlException ex)
            {
                if (i < 3)//&& ex.Number == -2           
                {
                    i++;
                    goto retry;
                }


            }
            catch (Exception ex)
            {

            }
            finally { }
            return lst;
        }
        public void addCompany(int ContentID, int ClientID)
        {
            int i = 0;
            retry:

            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    SqlCommand cmd = new SqlCommand("sp_Update_tpaw_Organization_Client", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("iiCondentID", SqlDbType.Int).Value = ContentID;
                    cmd.Parameters.Add("iiClientID", SqlDbType.Int).Value = ClientID;
                    cmd.CommandTimeout = 60;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    con.Close();
                    con.Dispose();
                }
            }
            catch
            {
                if (i < 3)//&& ex.Number == -2           
                {
                    i++;
                    goto retry;
                }


            }


        }
    }

}
